<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','UserController@index');

Route::get('/home','UserController@index');

Route::get('/login', 'UserController@logIn');

Route::get('/logout', 'UserController@logOut');

Route::post('/validar', 'UserController@validarLogIn');

Route::get('/profile', 'UserController@myProfile');

Route::get('/profile/{id}', 'UserController@profile');

Route::any('existenicknamee', function()
{
    return 'Hello World';
});

Route::post('/existenicknamee', 'UserController@existenickname');

Route::post('/existenicknamee/{id}', 'UserController@existenickname');

Route::get('/altaUsuario', 'UserController@altaUsuario');


