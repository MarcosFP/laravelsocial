function comprobarNickname(){
    var inputNickName = document.getElementById("inputNickName").value;
    var alert= "";
    if (inputNickName==""||inputNickName.length <3  || inputNickName.length >10 || ! /[A-Za-z]/.test(inputNickName) ){
        
        alert = "<div class='alert alert-dismissible alert-warning'  id='alertNombre'>"
        +"<button type='button' class='close closebtn' data-dismiss='alert'>&times;</button>"
        +"<strong>Error!</strong>"
        +"<a href='#' class='alert-link'>Este nickname no cumple con los requistos</a></div>" ;        
    }else{
        //Activamos CORS en el naegador
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        datos = {"nickname":inputNickName,
					"_token": '{{ csrf_token() }}'		};

        $.ajax({
            url: '/existenicknamee',
            type: 'POST',
            data: datos,
            dataType: 'JSON',
            success:function(respuesta){
                console.log(JSON.stringify(respuesta));

                    var repetido = respuesta.repetido;
                    
                    if (repetido =="false"){
                        alert = "<div class='alert alert-dismissible alert-success'  id='alertNombre'>"
                        +"<button type='button' class='close closebtn' data-dismiss='alert'>&times;</button>"
                        +"<strong>Error!</strong>"
                        +"<a href='#' class='alert-link'>Este nickname esta libre adelante.</a></div>" ;

                        $('#tag_id').html(alert);
                    }  
            },
            error:function(jqXHR, textStatus, errorThrown){

                if (jqXHR.status == 409)
                {
                    alert ="<div class='alert alert-dismissible alert-danger'  id='alertNombre'>"
                    +"<button type='button' class='close closebtn' data-dismiss='alert'>&times;</button>"
                    +"<strong>Error!</strong>"
                    +"<a href='#' class='alert-link'>Lo siento este nickname ya esta cogido por otro usuario.</a>"
                    +"Los nombres deben tener entre 5 y 20 carácteres"
                    +"</div>" ;
                    $('#tag_id').html(alert);      
                }
                else
                {
                    alert = "<div class='alert alert-dismissible alert-danger'  id='alertNombre'>"
                    +"<button type='button' class='close closebtn' data-dismiss='alert'>&times;</button>"
                    +"<strong>Error Interno!</strong>"
                    +"<a href='#' class='alert-link'>Ha ocurrido un error en el servidor.</a></div>" ;
    
                    $('#tag_id').html(alert);                  
                }
            }
        }).done(function(respuesta){

        });
    }
    $('#tag_id').html(alert);
}
