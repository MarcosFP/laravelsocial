<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserModuleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $this->get('/')
            ->assertStatus(200)
            ->assertSee('Do you dare to acept this challengue?');
    }
    
    public function testLogIn()
    {
        $this->get('/login?')
        ->assertStatus(200)
        ->assertSee('Write your Password');
    }
    
    public function testLogOut()
    {
        $this->get('/logout')
        ->assertStatus(302)
        ->assertLocation('/');
    }
    
    public function testValidarLogIn()
    {
        $data= [
            'nEmail' => 'marcos@marcos.com',
            'nPassword' => '123456'
        ];

        $this->post('/validar',$data)
        ->assertStatus(200)
        ->assertSee('Name from varible rendereing view :Marcos');
    }
    
    public function testMyProfileSuccesfull()
    {
        $this->withSession(['id' => '1'])
        ->get('/profile')
        ->assertStatus(200)
        ->assertSee('Name from varible rendereing view :Marcos');
    }
    
    
    public function testMyProfileWitiutSession()
    {       
        $this->withSession(['id' => '100'])
        ->get('/profile')
        ->assertStatus(302)
        ->assertLocation('/');
    }
    
    public function testMyProfileFailure()
    {
        $this->withSession(['id' => '100'])
        ->get('/profile')
        ->assertStatus(302)
        ->assertLocation('/');
    }
    
    public function testProfileSuccesfull()
    {
        $this->get('/profile/1')
        ->assertStatus(200)
        ->assertSee('Start adding your friends to your new RRSS');
    }
    
    public function testProfileFailure()
    {
        $this->get('/profile/200000')
        ->assertStatus(302)
        ->assertLocation('/');
    }
    
    public function testExisteNickNameSuccesfull()
    {
        $data= [
            'nickname' => 'MarcosPB'
        ];
        
        $this->post('/existenickname',$data)
        ->assertJson([
            'estado' => 'ok',
            'repetido' => "true"
        ])
        ->assertStatus(409);
    }

    public function testExisteNickNameFailure()
    {
        $data= [
            'nickname' => 'NicknameNuevo'
        ];
        
        $this->post('/existenickname',$data)
        ->assertJson([
            'estado' => 'ok',
            'repetido' => "false"
        ])
        ->assertStatus(200);
    }
    
    
    
    
}
