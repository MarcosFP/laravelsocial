<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','surname','email','nickname', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public static function findByEmail($email)
    {
        return static::where('email',$email)->first();
    }

    public static function findByNickname($nickname)
    {
        return static::where('nickname',$nickname)->first();
    }
    public static function findById($id)
    {
        return static::where("id",$id)->first();
    }

    public static function logIn($email,$password)
    {
        $usuario = User::findByEmail($email);

        if (password_verify($password, $usuario->password)){
            return array(
                "loged" => true,
                "user" => $usuario
                );
        }else{
            return array(
                "loged" => false,
                "user" => null
                );
        }
        
    }

}
