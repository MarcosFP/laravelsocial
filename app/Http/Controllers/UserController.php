<?php
namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index()
    {
        if (null !== (Session::get('id'))){

            return view('timeline');
        }else{
            return view('welcome');
        }
    }

    public function profile($id)
    {
        $user = User::findById($id);
        if ($user !== null){
            return view('timeline');
        }else{
            return redirect('/');
        }
    }

    public function myProfile()
    {
        if (null !== (Session::get('id'))){
            $userdto = User::find(Session::get('id'));
            if ($userdto != null){
                return response()->view('profile', compact("userdto"));
            }else {
                return redirect('/');
            }      
        }else{
            return redirect('/');
        }
    }

    public function logIn()
    {
        return view('login');
    }

    public function logout()
    {
        Session::flush();
        return redirect('/');
    }

    public function validarLogIn(Request $req)
    {
        
        if (empty($req->input('nEmail')) || empty($req->input('nPassword'))){
            return response()->view('login')->setStatusCode(404);
        }
        
        $email = $req->input('nEmail');
        $password= $req->input('nPassword');  
        
        $user = User::logIn($email,$password);
        $userdto = $user["user"];
        if ($user['loged']){
            
            Session::put('id', $userdto->id );
            Session::put('name', $userdto->name );
            Session::put('surname', $userdto->surname );
            Session::put('email', $userdto->email );
            Session::put('nickname', $userdto->nickname );
   
            return response()->view('profile', compact("userdto"));
        }
        return response()->view('login')->setStatusCode(404);
    }

    public function existenickname(Request $req)
    {
        $nickname = $req->input('nickname');
        $user = User::findByNickname($nickname);

        if ($user != null){
            
            return response()->json([
                'estado' => 'ok',
                'repetido' => "true"
            ])->setStatusCode(409);

        }else{
            return response()->json([
                'estado' => 'ok',
                'repetido' => "false"
            ])->setStatusCode(200);
       
        }
    }
    
    
    public function altaUsuario()
    {
        return view('register');
        
     
    }
    
    
    
    
}