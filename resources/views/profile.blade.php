<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Custom CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/validarNickname.js"></script>

    <meta charset="utf-8">
    <title>Login</title>
    
</head>
<body>
    <!-- Incluimos la barra de navegación -->
    @include('navbar/navbar_load')

    <div id="body">

        <h4> Data retrieved from variables passed before rendereing the view: </h4>
        Name from varible rendereing view :{!! $userdto->name !!} <br>
        Surname from varible rendereing view :{!! $userdto->surname !!} <br>
        Email from varible rendereing view :{!! $userdto->email !!} <br>
        Nickname from varible rendereing view :{!! $userdto->nickname !!} <br>

        <br>
        <br>
        <br>
        <h4> Data retrieved from Session: </h4>
        Name from Session :{{ Session::get('name') }} <br>
        Surname from Session : {{ Session::get('surname') }} <br>
        Email from session: {{ Session::get('email') }} <br>
        Nickname from session :{{ Session::get('nickname') }} <br>

        <br>
        <br>
        <br>
        <h4> Check if the nickname is avaible: </h4>

        <div class="form-group has-success" style="width: 30%">
            <label class="form-control-label" for="inputNickName">Nickname:</label>
            @csrf
            <input type="text"  class="form-control is-valid" id="inputNickName" onchange="comprobarNickname()">
        </div>
        <div id="tag_id"></div>
        
    </div>

</body>
<html>
