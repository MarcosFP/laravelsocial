
{{--
En caso de que exista la variable de session cargamos la barra de navegacion para usuarios reguistrados
en caso contrario la destinada para usuarios publicos
--}}

@if(null !== (Session::get('nickname')))
        
    @include('navbar\navbar_user')
@else
    @include('navbar\navbar_public')
@endif