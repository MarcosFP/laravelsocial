<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Custom CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <meta charset="utf-8">
    <title>Login</title>
</head>

<body>
    <!-- Incluimos la barra de navegación 
    @include('navbar/navbar_load')-->

    
    <div id="body">
        <div class="jumbotron">
            <h1 class="display-3">Welcome dear visitor!</h1>
            <p class="lead">This is a simple sample to show your skills in PHP.</p>
            <hr class="my-4">
            <p>Do you dare to acept this challengue?</p>
            <p class="lead">
                <a class="btn btn-primary btn-lg" href="login" role="button">Yes, I dare</a>
            </p>
        </div>
    </div>

</body>

</html>