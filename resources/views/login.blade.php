<html lang="es">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Custom CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

   <title>Login</title>
</head>
<body>

    <!-- Incluimos la barra de navegación -->
    @include('navbar/navbar_load')
    
    <div id="body">
        <div class="modal; visibility: visible;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Login</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    {!! Form::open(['url' => 'validar']) !!}

                    <form method="POST" action="validar">
                        <div class="modal-body">
                            <div class="form-group">
                                    {!! Form::label('nEmail', 'Email:') !!}
                                    {!! Form::text('nEmail', null, ['class' => 'form-control',"placeholder"=>"example@email.com"]) !!}
                            </div>
                            <div class="form-group">
                                    {!! Form::label('nPassword', 'Password:') !!}
                                    {!! Form::text('nPassword', null, ['class' => 'form-control',"placeholder"=>"Write your Password"]) !!}

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary">Register</button>
                            
                            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                            
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</body>
<html>
